import { Homebanking } from '../homebanking/Homebanking';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/index.css';

var current_user = Homebanking.getColection("current_user");
console.log(current_user);

let numTarjetaCredito = current_user.cliente.tarjetaCredito.numTarjetaCredito.split("-");
document.getElementById("tarjeta").innerHTML = `Visa XXXX-XXXX-XXXX-${numTarjetaCredito[3]}`;
document.getElementById("thru").innerHTML = `Fecha vencimiento: ${current_user.cliente.tarjetaCredito.thru} `;
document.getElementById("saldo-tarjeta").innerHTML = `$ ${current_user.cliente.tarjetaCredito.saldo} `;
document.getElementById("referencia-pago").innerHTML = `Referencia de pago: ${current_user.cliente.tarjetaCredito.referenciaPago}`;

document.getElementById("tabla").innerHTML = "";
current_user.cliente.tarjetaCredito.movimientosTarjetaCredito.forEach(function (element, index){
	document.getElementById("tabla").innerHTML += `
		<tr>
	        <th scope="row">${element.fecha}</th>
	        <td>${element.comercio}</td>
	        <td>$ ${element.importe}</td>
	    </tr>
	`;
});

// Evento que realiza un pago de tarjeta de crédito
document.getElementById("btnSolicitarPago").addEventListener("click", () => {
    if (!current_user.cliente.tarjetaCredito.referenciaPago == parseInt(document.getElementById("pago-referencia").value))
        swal("Error!", "El número de referencia de pago es incorrecto", "error");
    if (Cliente.addMovCajaAhorro(current_user.numDocument,
                                "PAGO TARJETA CRÉDITO",
                                document.getElementById("pago-importe").value,
                                -1)){ 
        Homebanking.updateCurrentUser(current_user.numDocument);                                    
        swal("Muchas gracias!", "Su pago fue efectuado con éxito.", "success")
        .then(() => {
            window.location.href = "tarjeta.html";
        });} else {
        swal("Error!", "Ha ocurrido un error al realizar el pago. Verifique que los datos ingresados sean correctos.", "error")
        .then(() => {
            window.location.href = "tarjeta.html";
        });
    };
});

// Evento que cierra la sesión de un usuario
document.getElementById("btnLogOut").addEventListener("click", () => {
    Homebanking.logOutUser();
    window.location.href = "index.html";
});