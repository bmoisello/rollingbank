import { Cliente } from './Cliente';
import { CajaAhorro } from './CajaAhorro';

export class Homebanking{
    constructor(){}

    // Trae la colección del local storage
    static getColection(p_key){
        return JSON.parse(localStorage.getItem(p_key)) || [];
    }

    // Inserta la colección en el local storage
    static setColection(p_key, p_colection){
        localStorage.setItem(p_key, JSON.stringify(p_colection));
    }

    // Elimina la colección del local storage
    static removeColection(p_key){
        localStorage.removeItem(p_key);
    }

    // Verifica si existe otra persona con el mismo numero de documento
    static verifyNumDocument(p_numDocument){
        let colectionClientes = Homebanking.getColection('clients');
        colectionClientes = colectionClientes.find(element => {
            return element.numDocument == p_numDocument;
        });
        return colectionClientes ? true : false;
    }

    // Verifica si existe otra persona con el mismo nombre de usuario
    static verifyUserName(p_userName){
        let colectionClientes = Homebanking.getColection('clients');
        colectionClientes = colectionClientes.find(element => {
            return element.cliente.userName == p_userName;
        });
        return colectionClientes ? true : false;
    }

    // Método que verifica que usuario y contraseña sean válidos
    static verifyUserLogin(p_numDocument, p_userName, p_password){
        let colectionClientes = Homebanking.getColection('clients');
        let current_user = colectionClientes.find(element => {
            return element.numDocument == p_numDocument && element.cliente.userName == p_userName 
                    && element.cliente.password == p_password && element.cliente.estado == "Aprobado";
        });
        // Si se encontró un cliente con los datos ingresados para el inicio de sesión los coloca en el localStorage current_user
        if(current_user){
            Homebanking.setColection('current_user', current_user); 
            return true;
        } else {
            console.log('Los datos ingresados son incorrectos');        
            return false;
        } 
    }

    // Método que actualiza la información del current_user
    static updateCurrentUser(p_numDocument){
        Homebanking.setColection("current_user", Homebanking.getColection("clients").find(element => {
            return element.numDocument == p_numDocument;
        }));
    }
    // Método de cierre sesión
    static logOutUser(){
        Homebanking.removeColection('current_user');
    }

    // Método que retorna todos los clientes pendientes de aprobación
    static getClientsPend(){
        return Homebanking.getColection("clients").filter(element => {
            return element.cliente.estado == "Pendiente";
        });
    }

    // Método que retorna todos las tarjetas de crédito pendientes de aprobación
    static getTarjetasCreditoPend(){
        return Homebanking.getColection("clients").filter(element => {
            return element.cliente.tarjetaCredito.estado == "Pendiente";
        });
    }

    // Método que retorna todos los prestamos pendientes de aprobación de un cliente
    static getPrestamosPend(p_numDocument){
        let colectionClients = Homebanking.getColection("clients");
        let client = colectionClients.find(element => {
            return element.numDocument == p_numDocument;
        });
        return client.cliente.prestamos.filter(element => {
            return element.estado == "Pendiente"; 
        });    
    }

    // Método que retorna todos los prestamos pendientes de aprobación de un cliente
    static getClientsPrestamosPend(){
        return Homebanking.getColection("clients").filter(element => {
            return element.cliente.requestLoan;
        });
    }

    // Método que retorna la cantidad de préstamos que tiene un cliente
    static getCantPrestamosClient(p_numDocument){
        let client = Homebanking.getColection("clients").find(element => {
            return element.numDocument == p_numDocument;
        });
        return client.cliente.prestamos.length;
    }
}