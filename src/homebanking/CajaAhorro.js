import { Homebanking } from "./Homebanking";

export class CajaAhorro {
    /*
        nroCuenta
        saldo
        movimientosCajaAhorro
    */
    constructor(p_numDocument){
        this.nroCuenta = p_numDocument + '/1';
        this.cbu = Math.round(Math.random()*99999999999999999999);
        this.saldo = 0;
        this.movimientosCajaAhorro = [];
    }
    
    // Método que trae los datos de la caja de ahorro instanciada al crearla
    getCajaAhorro(){
        return {
            nroCuenta: this.nroCuenta,
            cbu: this.cbu,
            saldo: this.saldo,
            movimientosCajaAhorro: this.movimientosCajaAhorro
        };
    }

    static addMovimiento(p_concepto, p_importe, p_fecha, p_signo){
        return {
            concepto: p_concepto,
            importe: p_importe,
            fecha: p_fecha,
            signo: p_signo
        }
    }

    static calcSaldo(p_movimientos){
        let saldo = 0;
        p_movimientos.forEach(element => {
            saldo += element.importe * element.signo; 
        });
        return saldo;
    }
}