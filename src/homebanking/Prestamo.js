import { Homebanking } from "./Homebanking";

export class Prestamo{
    /*
        numDocument,
        montoPrestado,
        interes,
        fechaPrestamo,
        cantCuotas,
        liqCuotas[]
    */
    constructor(p_props){
        this.props = p_props;
        this.nroPrestamo = this.props.numDocument + '/' + (Homebanking.getCantPrestamosClient(this.props.numDocument) + 1);
        this.montoAPagar = this.props.montoPrestado * (1+((this.props.interes/100/12) * this.props.cantCuotas));
        this.saldo = this.montoAPagar;
        this.estado = "Pendiente";
        this.motivo = p_props.motivo;
        this.fechaVencimiento = new Date(new Date(this.props.fechaPrestamo).getTime() + (this.props.cantCuotas*30*24*60*60*1000)); 
        this.liqCuotas = Prestamo.getCuotas(this.props.cantCuotas, this.props.fechaPrestamo, this.montoAPagar);
    }

    getPrestamo(){
        return {
            numDocument: this.props.numDocument,
            nroPrestamo: this.nroPrestamo,
            montoPrestado: parseFloat(this.props.montoPrestado).toFixed(2),
            montoAPagar: parseFloat(this.montoAPagar).toFixed(2),
            saldo: parseFloat(this.saldo).toFixed(2),
            estado: this.estado,
            interes: parseFloat(this.props.interes).toFixed(2),
            fechaPrestamo:  new Date(this.props.fechaPrestamo).toISOString().replace(/T.*/,'').split('-').reverse().join('/'),
            cantCuotas: this.props.cantCuotas,
            fechaVencimiento: new Date(this.fechaVencimiento).toISOString().replace(/T.*/,'').split('-').reverse().join('/'),
            motivo: this.motivo,
            liqCuotas: this.liqCuotas
        };
    }

    static getCuotas(p_cantCuotas, p_fechaPrestamo, p_montoAPagar){
        let cuotas = [];
        let fechaCuota = p_fechaPrestamo;
        let montoAPagar = p_montoAPagar / p_cantCuotas;
        for (let i=0; i < p_cantCuotas; i++){
            fechaCuota = new Date(new Date(fechaCuota).getTime() + (30*24*60*60*1000))
            let cuota = {
                numCuota: i+1,
                diaPago: new Date(fechaCuota).toISOString().replace(/T.*/,'').split('-').reverse().join('/'),
                estado: "Adeudada",
                montoPago: parseFloat(montoAPagar).toFixed(2)
            };
            cuotas.push(cuota);
        }
        return cuotas;
    }
}