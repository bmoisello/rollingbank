const path = require('path');
const webpack = require('webpack');

/*
 * SplitChunksPlugin is enabled by default and replaced
 * deprecated CommonsChunkPlugin. It automatically identifies modules which
 * should be splitted of chunk by heuristics using module duplication count and
 * module category (i. e. node_modules). And splits the chunks…
 *
 * It is safe to remove "splitChunks" from the generated configuration
 * and was added as an educational example.
 *
 * https://webpack.js.org/plugins/split-chunks-plugin/
 *
 */

const HtmlWebpackPlugin = require('html-webpack-plugin');

/*
 * We've enabled HtmlWebpackPlugin for you! This generates a html
 * page for you when you compile webpack, which will make you start
 * developing and prototyping faster.
 *
 * https://github.com/jantimon/html-webpack-plugin
 *
 */

const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
	mode: 'development',
	entry: {
		index: './src/home/index.js',
		homebanking: './src/homebanking/index.js',
		admin: './src/administration/index.js',
		prestamos: './src/prestamos/index.js',
		tarjeta: './src/tarjeta/index.js',
		caja: './src/caja/index.js',
		movimientos: './src/movimientos/index.js'
	},

	output: {
		filename: '[name].[chunkhash].js',
		path: path.resolve(__dirname, 'dist')
	},

	plugins: [
		new webpack.ProgressPlugin(), 
		new HtmlWebpackPlugin({
			template: 'src/home/index.html',
			chunks: ['index'],
			filename: 'index.html'
		}),
		new HtmlWebpackPlugin({
			template: 'src/homebanking/index.html',
			chunks: ['homebanking'],
			filename: 'homebanking.html'
		}),
		new HtmlWebpackPlugin({
			template: 'src/administration/index.html',
			chunks: ['admin'],
			filename: 'admin.html'
		}),
		new HtmlWebpackPlugin({
			template: 'src/prestamos/index.html',
			chunks: ['prestamos'],
			filename: 'prestamos.html'
		}),
		new HtmlWebpackPlugin({
			template: 'src/tarjeta/index.html',
			chunks: ['tarjeta'],
			filename: 'tarjeta.html'
		}),
		new HtmlWebpackPlugin({
			template: 'src/caja/index.html',
			chunks: ['caja'],
			filename: 'caja.html'
		}),
		new HtmlWebpackPlugin({
			template: 'src/movimientos/index.html',
			chunks: ['movimientos'],
			filename: 'movimientos.html'
		}),
		new CopyWebpackPlugin([{
		     from:'./src/homebanking/img',		to:'images'   
		}]),
		new CopyWebpackPlugin([{
		     from:'./src/home/img',		to:'images'   
		}]),
		new CopyWebpackPlugin([{
		     from:'./src/tarjeta/img',		to:'images'   
		}]),
		new CopyWebpackPlugin([{
		     from:'./src/caja/img',		to:'images'   
		}]),
		new CopyWebpackPlugin([{
		     from:'./src/prestamos/img',		to:'images'   
		}]),
	],

	module: {
		rules: [
			{
				test: /.(js|jsx)$/,
				include: [path.resolve(__dirname, 'src')],
				loader: 'babel-loader',

				options: {
					plugins: ['syntax-dynamic-import'],

					presets: [
						[
							'@babel/preset-env',
							{
								modules: false
							}
						]
					]
				}
			},
			{
				test: [/.css$/],                
				  use:[                    
				   'style-loader',                  
				   'css-loader'
				  ]
			}
		]
	},

	optimization: {
		splitChunks: {
			cacheGroups: {
				vendors: {
					priority: -10,
					test: /[\\/]node_modules[\\/]/
				}
			},

			chunks: 'async',
			minChunks: 1,
			minSize: 30000,
			name: true
		}
	},

	devServer: {
		open: true
	}
};

